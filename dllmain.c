#include <windows.h>
#include <string.h>

void* explosion_fire = (void*)0x0073736E;
void* creeping_fire  = (void*)0x0053AAB0;

void copy(void* ptr, const char bytes[], size_t size) {
    DWORD old_prot;

    VirtualProtect(ptr, size, PAGE_READWRITE, &old_prot);
    memcpy(ptr, bytes, size);
    VirtualProtect(ptr, size, old_prot, &old_prot);
}

int __stdcall DllMain(HMODULE module, DWORD reason, LPVOID reserved) {
    
    switch (reason) {
        case DLL_PROCESS_ATTACH: {
            if (GetModuleHandleA("samp.dll")) {
                copy(explosion_fire, "\xEB", 1);
                copy(creeping_fire, "\xE9\x6D\x01\x00\x00\x90", 6);
            }
            break;
        }
        case DLL_PROCESS_DETACH: {
            copy(explosion_fire, "\x74", 1);
            copy(creeping_fire, "\x0F\x8E\x6C\x01\x00\x00", 6);
            break;
        }
    };
    return 1;
}
